// using a boilerplate I have made in the past for this server

// DEPENDENCIES 
const colors = require('colors');
const logger = require('morgan');
const express = require('express');
const path = require('path');

// SERVER VARIABLES
const HOST = 'localhost';
const PORT = 5000;
const SERVER_NAME = "EXPENSE_TRACKER";
const db = require('./models/index');

// CONSOLE COLORS
colors.setTheme({
  error: 'red',
  warn: 'yellow',
  link: ['cyan', 'underline'],
  info: ['black', 'bold', 'bgWhite', 'italic'],
});

// TIMEOUT function - fires every time a timeout of 30s is hit on the server
const haltOnTimedout = (req, res, next) => {
  if (!req.timedout) next();
}

// SERVER SETUP
const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(haltOnTimedout);
app.use(express.urlencoded({ extended: true }));
app.use(haltOnTimedout);

// ROUTES
const routes = require(path.join(__dirname, './routes'));
app.use(routes);

// SERVER START
db.sequelize.sync({ force: true }).then(function () {
  app.listen(PORT, () => {
    console.log(' ');
    console.log(` ${SERVER_NAME} `.info + '\n');
    console.log(` 📡  Server ready at ` + `http://${HOST}:${PORT}`.link + '\n');
  });
});
