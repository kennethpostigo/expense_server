# expense_server
server/backend for [expense_tracker react native app](https://bitbucket.org/kennethpostigo/expense_server/src/master/)

### Usage
This server is made using express, mysql, and sequelize.
In order to run, use `node server`

In `server.js`:
`db.sequelize.sync({ force: true })` causes the database to be erased on startup

If you want a persistent database, use `db.sequelize.sync({ force: false })`.

### Notes
Must have a mysql server running. If launching server fails to start, `^C` to shut down then run `node server` again.  Normally, sequelize does not create the nnecessary database(s) to run the server, but I included a small function within `models/index.js` to initiate the 'EXPENSE_TRACKER_DEV' database mentioned in `config/config.json`
