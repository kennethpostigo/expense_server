const db = require('../models');

module.exports = {
  findAll: (req, res) => {
    db.Expense
      .findAll()
      .then(expenses => res.json(expenses))
      .catch(err => {
        console.error(err);
        res.status(500).json(err);
      });
  },
  findByCat: (req, res) => {
    db.Expense
      .findAll({ where: { category: req.params.category } })
      .then(expenses => res.json(expenses))
      .catch(err => {
        console.error(err);
        res.status(500).json({ msg: 'COULD NOT FIND EXPENSES', err });
      });
  },
  create: (req, res) => {
    db.Expense
      .create(req.body)
      .then(expense => res.json(expense))
      .catch(err => {
        console.error(err);
        res.status(500).json({ msg: 'COULD NOT CREATE EXPENSE', err });
      });
  },
  update: (req, res) => {
    db.Expense
      .update(req.body, { where: { id: req.params.id } })
      .then(expense => res.json(expense))
      .catch(err => {
        console.error(err);
        res.status(500).json({ msg: 'COULD NOT UPDATE EXPENSE', err });
      });
  },
};
