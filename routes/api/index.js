const router = require('express').Router();

const expenseRoutes = require('./expense');

// test message
router.get('/', (req, res) => {
  res.json({ data: 'EXPENSE_TRACKER API'});
});

router.use(expenseRoutes);

module.exports = router;
