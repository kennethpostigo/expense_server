const router = require('express').Router();
const expenseController = require('../../controllers/expense');

// get all expenses
router.get('/expenses', expenseController.findAll);
// get expenses by category
router.get('/expenses/:category', expenseController.findByCat);
// create new expense
router.post('/expense', expenseController.create);
// update expense (not included in app)
router.put('/expense/:id', expenseController.update);

module.exports = router;
